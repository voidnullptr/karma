// Property of Erick Pombo Sonderblohm

#include "Karma.h"
#include "BaseProjectile.h"
#include "DamageInterface.h"


// Sets default values
ABaseProjectile::ABaseProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	Bullet = CreateDefaultSubobject<USphereComponent>(TEXT("Bullet"));
	Bullet->OnComponentHit.AddDynamic(this, &ABaseProjectile::OnHit);
	this->SetRootComponent(Bullet);

	BlasterRay = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("BulletParticle"));
	BlasterRay->AttachToComponent(Bullet, FAttachmentTransformRules::KeepRelativeTransform);

	ExplosionForce = CreateDefaultSubobject<URadialForceComponent>(FName("Explosion Force"));
	ExplosionForce->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
	ProjectileMovement->bAutoActivate = false;
}

// Called when the game starts or when spawned
void ABaseProjectile::BeginPlay()
{
	Super::BeginPlay();
	SetLifeSpan(TimeToDieInSecs);
}

// Called every frame
void ABaseProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABaseProjectile::LaunchProjectile(float Speed)
{
	ProjectileMovement->SetVelocityInLocalSpace(FVector::ForwardVector * Speed);
	ProjectileMovement->Activate();
}

void ABaseProjectile::SetDamageInfo(float _DamageMin, float _DamageMax, float _CritDamageMultiplier, float _CritChance)
{
	DamageMin = _DamageMin;
	DamageMax = _DamageMax;
	CritChance = _CritChance;
	CritDamageMultiplier = _CritDamageMultiplier;
}

void ABaseProjectile::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	// Spawn blast effect
	UGameplayStatics::SpawnEmitterAtLocation(
		this,
		ImpactBlast,
		Hit.ImpactPoint,
		GetActorRotation(),
		true
	);
	
	// Fire impulse
	ExplosionForce->FireImpulse();

	// Play sound at location
	UGameplayStatics::PlaySoundAtLocation(this, ImpactSound, Hit.ImpactPoint);

	// Apply damage
	IDamageInterface* ReactingObject = Cast<IDamageInterface>(OtherActor); // ReactingObject will be non-null if OriginalObject implements UReactToTriggerInterface.
	if (ReactingObject)
	{
		ReactingObject->TakeDamage(DamageMin, DamageMax, CritDamageMultiplier, CritChance, Hit.BoneName, GetInstigator());
	}

	Destroy();
}

