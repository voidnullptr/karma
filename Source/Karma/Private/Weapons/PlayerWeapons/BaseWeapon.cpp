// Property of Erick Pombo Sonderblohm

#include "Karma.h"
#include "BaseWeapon.h"
#include "WeaponFireComponent.h"
#include "WeaponHeatComponent.h"
#include "WeaponRecoilComponent.h"

// Sets default values
ABaseWeapon::ABaseWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	RootComponent = WeaponMesh;
}

// Called when the game starts or when spawned
void ABaseWeapon::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ABaseWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABaseWeapon::FireWeapon(FVector HitLocation)
{
	
	auto FireComponent = GetFireComponent();
	auto HeatComponent = GetHeatComponent();
	auto RecoilComponent = GetRecoilComponent();

	// Call fire on our FireWeaponComponent
	if (FireComponent)
	{
		// Add Heat to our weapon
		if (HeatComponent)
		{
			if (!HeatComponent->GetIsOverHeated())
			{
				FireComponent->Fire(HitLocation);
				HeatComponent->FireHeatBuildUp();
				if (RecoilComponent)
				{
					RecoilComponent->ApplyRecoil();
				}
			}
		}
		else
		{
			FireComponent->Fire(HitLocation);
			if (RecoilComponent)
			{
				RecoilComponent->ApplyRecoil();
			}
		}
	}
}

UWeaponFireComponent* ABaseWeapon::GetFireComponent() const
{
	auto FireComponent = FindComponentByClass<UWeaponFireComponent>();
	return FireComponent;
}

UWeaponHeatComponent* ABaseWeapon::GetHeatComponent() const
{
	auto HeatComponent = FindComponentByClass<UWeaponHeatComponent>();
	return HeatComponent;
}

UWeaponRecoilComponent* ABaseWeapon::GetRecoilComponent() const
{
	auto RecoilComponent = FindComponentByClass<UWeaponRecoilComponent>();
	return RecoilComponent;
}

float ABaseWeapon::GetZoomFOV() const
{
	return ZoomFOV;
}

