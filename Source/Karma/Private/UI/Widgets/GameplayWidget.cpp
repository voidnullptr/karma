// Property of Erick Pombo Sonderblohm

#include "Karma.h"
#include "GameplayWidget.h"
#include "KPlayerCharacter.h"
#include "BaseWeapon.h"
#include "WeaponHeatComponent.h"


void UGameplayWidget::NativeConstruct()
{
	Super::NativeConstruct();
	AKPlayerCharacter* KPC = Cast<AKPlayerCharacter>(GetOwningPlayerPawn());

	if (KPC)
	{
		KPC->ShowHideCrosshairsEvent.AddDynamic(this, &UGameplayWidget::OnShowHideCrosshairs);
		UWeaponHeatComponent* HeatComponent = KPC->GetWeapon()->GetHeatComponent();
		if (HeatComponent)
		{
			HeatComponent->WeaponOverheatEvent.AddDynamic(this, &UGameplayWidget::OnWeaponOverheated);
			HeatComponent->WeaponOverheatPercent.AddDynamic(this, &UGameplayWidget::OnWeaponOverheatedPercent);
		}
	}
}
