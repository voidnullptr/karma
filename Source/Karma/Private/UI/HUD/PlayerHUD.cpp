// Property of Erick Pombo Sonderblohm

#include "Karma.h"
#include "PlayerHUD.h"
#include "Runtime/UMG/Public/UMG.h"


APlayerHUD::APlayerHUD()
{

}

void APlayerHUD::BeginPlay()
{
	Super::BeginPlay();
	WI_GameplayWidget = CreateWidget<UUserWidget>(GetOwningPlayerController(), WP_GameplayWidget);
	WI_GameplayWidget->AddToViewport();
}
