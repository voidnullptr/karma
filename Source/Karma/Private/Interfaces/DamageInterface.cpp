// Property of Erick Pombo Sonderblohm

#include "Karma.h"
#include "DamageInterface.h"


// This function does not need to be modified.
UDamageInterface::UDamageInterface(const class FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}

// Add default functionality here for any IDamageInterface functions that are not pure virtual.
