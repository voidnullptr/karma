// Property of Erick Pombo Sonderblohm

#include "Karma.h"
#include "BaseEnemyCharacter.h"
#include "Perception/PawnSensingComponent.h"
#include "KPlayerCharacter.h"


// Sets default values
ABaseEnemyCharacter::ABaseEnemyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	PawnSensingComponent = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComponent"));
	Waypoint_1 = CreateDefaultSubobject<UBillboardComponent>(TEXT("Waypoint_1"));
	Waypoint_1->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform);
	Waypoint_1->SetVisibility(false);
	Waypoint_1->SetRelativeLocation(FVector(150.0f, 0.0f, 0.0f));

	Waypoint_2 = CreateDefaultSubobject<UBillboardComponent>(TEXT("Waypoint_2"));
	Waypoint_2->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform);
	Waypoint_2->SetVisibility(false);
	Waypoint_2->SetRelativeLocation(FVector(-150.0f, 0.0f, 0.0f));

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	WeaponMesh->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform);

	MuzzleArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("MuzzleArrow"));
	MuzzleArrow->AttachToComponent(WeaponMesh, FAttachmentTransformRules::KeepRelativeTransform);
}

void ABaseEnemyCharacter::TakeDamage(float _DamageMin, float _DamageMax, float _CritDamageMultiplier, float _CritChance, FName HitBoneName, AActor* InstigatorActor)
{
	// UE_LOG(LogTemp, Warning, TEXT("ABaseEnemyCharacter::TakeDamage"));

	APawn* PawnInstigator = Cast<APawn>(InstigatorActor);
	if (PawnInstigator)
	{
		SetToCombat(PawnInstigator);
	}

	CalculateHealth(_DamageMin, _DamageMax, _CritChance, _CritDamageMultiplier, HitBoneName);

	if (bIsDead)
	{
		// Kill our actor
		Destroy();
	}
}

void ABaseEnemyCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (!ensure(PawnSensingComponent)) { return; }
	PawnSensingComponent->OnSeePawn.AddDynamic(this, &ABaseEnemyCharacter::OnSeePawn);
	PawnSensingComponent->OnHearNoise.AddDynamic(this, &ABaseEnemyCharacter::OnHearNoise);
}

void ABaseEnemyCharacter::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	// Update Billboard Visibility
	Waypoint_1->SetVisibility(CombatStatus == ECombatStatus::Patrol);
	Waypoint_2->SetVisibility(CombatStatus == ECombatStatus::Patrol);

	// Change the weapon type depending on Combat Type
	if (!ensure(AssaultWeaponMesh)) { return; }
	if (!ensure(CoverWeaponMesh)) { return; }
	if (!ensure(HeavytWeaponMesh)) { return; }

	switch (CombatType)
	{
	case ECombatType::Assault:
		WeaponMesh->SetSkeletalMesh(AssaultWeaponMesh);
		break;
	case ECombatType::Cover:
		WeaponMesh->SetSkeletalMesh(CoverWeaponMesh);
		break;
	case ECombatType::Heavy:
		WeaponMesh->SetSkeletalMesh(HeavytWeaponMesh);
		break;
	}

	// Attach weapon to Skeletal Socket
	WeaponMesh->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, WeaponSocket);

	// Arrow component required to aim
	MuzzleArrow->AttachToComponent(WeaponMesh, FAttachmentTransformRules::SnapToTargetIncludingScale, MuzzleSocket);
}

// Called when the game starts or when spawned
void ABaseEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	CurrentHealth = MaxHealth;

	// Get locations for waypoint
	Waypoint_1_Location = Waypoint_1->GetComponentLocation();
	Waypoint_2_Location = Waypoint_2->GetComponentLocation();
}

// Called every frame
void ABaseEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABaseEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ABaseEnemyCharacter::CalculateHealth(float _DamageMin, float _DamageMax, float _CritDamageMultiplier, float _CritChance, FName HitBoneName)
{
	if (bIsDead) { return; }

	auto localDamage = FPlatformMath::RoundToInt(FMath::RandRange(_DamageMin, _DamageMax));
	auto randomCritChance = FPlatformMath::RoundToInt(FMath::RandRange(0.0f, 100.0f));

	if (randomCritChance <= _CritChance)
	{
		// if this happens apply critical damage
		localDamage *= _CritDamageMultiplier;
	}

	// check bone name hit, check for head shot
	if (HitBoneName == "head")
	{
		localDamage *= _CritDamageMultiplier;
	}

	// subtract the current health with the local damage
	CurrentHealth -= localDamage;
	if (CurrentHealth < 0) { CurrentHealth = 0.0f; }

	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("ABaseEnemyCharacter::CalculateHealth - CurrentHealth: %f"), CurrentHealth));
	}

	// Check for death
	bIsDead = CurrentHealth <= 0;

}

void ABaseEnemyCharacter::OnSeePawn(APawn* _Pawn)
{
	UE_LOG(LogTemp, Warning, TEXT("ABaseEnemyCharacter::OnSeePawn"));

	SetToCombat(_Pawn);
}

void ABaseEnemyCharacter::SetToCombat(APawn* _Pawn)
{
	if (CombatStatus != ECombatStatus::Combat)
	{
		AKPlayerCharacter* KPC = Cast<AKPlayerCharacter>(_Pawn);
		if (KPC)
		{
			PlayerCharacter = KPC;
			CombatStatus = ECombatStatus::Combat;
		}
	}
}

void ABaseEnemyCharacter::OnHearNoise(APawn* _Instigator, const FVector& _Location, float _Volume)
{
	UE_LOG(LogTemp, Warning, TEXT("ABaseEnemyCharacter::OnHearNoise"));
	if (GEngine)
	{
		const FString VolumeDesc = FString::Printf(TEXT(" at volume %f"), _Volume);
		FString message = TEXT("Heard Actor ") + _Instigator->GetName() + VolumeDesc;
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, message);
	}

	SetToCombat(_Instigator);
}

