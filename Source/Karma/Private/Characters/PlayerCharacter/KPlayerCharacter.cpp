// Property of Erick Pombo Sonderblohm

#include "Karma.h"
#include "KPlayerCharacter.h"
#include "KPlayerController.h"
#include "BaseWeapon.h"
#include "Kismet/KismetMathLibrary.h"
#include "SlateWrapperTypes.h"

// Sets default values
AKPlayerCharacter::AKPlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 200.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Zoom
	ZoomTimeline = CreateDefaultSubobject<UTimelineComponent>(TEXT("ZoomTimeline"));
	ZoomInterpFunction.BindUFunction(this, FName{ TEXT("ZoomTimelineFloatReturn") });

	// Camera
	ToggleCameraTimeline = CreateDefaultSubobject<UTimelineComponent>(TEXT("ToggleCameraTimeline"));
	ToggleCameraInterpFunction.BindUFunction(this, FName{ TEXT("ToggleCameraTimelineFloatReturn") });
}

// Called when the game starts or when spawned
void AKPlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	EquipWeapon(WeaponToSpawnBP);
	ZoomTimeline->AddInterpFloat(ZoomCurve, ZoomInterpFunction, FName{ TEXT("RecoilInterp") });
	ToggleCameraTimeline->AddInterpFloat(ToggleCameraCurve, ToggleCameraInterpFunction, FName{ TEXT("ToggleCameraInterp") });
	socketOffset = CameraBoom->SocketOffset;
}

// Called every frame
void AKPlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AKPlayerCharacter::OnCoverAvailable(bool _bIsCoverAvailable, bool _bIsHighCover, FRotator _CoverRotation)
{
	//UE_LOG(LogTemp, Warning, TEXT("AKPlayerCharacter::OnCoverAvailable"));
	if (!bIsAiming)
	{
		CoverAvailable = _bIsCoverAvailable;
		CoverRotation = _CoverRotation;
		bIsHighCover = _bIsHighCover;

		if (!CoverAvailable)
		{
			SetIsTakingCover(false);
		}
	}
}

void AKPlayerCharacter::EndCover(bool _bEndCoverLeft, bool _bEndCoverRight)
{
	//UE_LOG(LogTemp, Warning, TEXT("AKPlayerCharacter::EndCover"));
	EndofCoverLeft = _bEndCoverLeft;
	EndofCoverRight = _bEndCoverRight;
}

void AKPlayerCharacter::EquipWeapon(TSubclassOf<ABaseWeapon> _WeaponToSpawnBP)
{
	if (!ensure(_WeaponToSpawnBP)) { return; }

	// First check if we have a weapon, de-spawn it first
	if (Weapon != nullptr)
	{
		// TODO: Play unequip animation and destroy
		Weapon->Destroy();
	}

	// Spawn Weapon
	FActorSpawnParameters SpawnParams;
	SpawnParams.Instigator = this;
	Weapon = GetWorld()->SpawnActor<ABaseWeapon>(_WeaponToSpawnBP->GetOwnerClass(), FVector(ForceInitToZero), FRotator(ForceInitToZero), SpawnParams);
	Weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, WeaponSocket);
}

ABaseWeapon* AKPlayerCharacter::GetWeapon() const
{
	return Weapon;
}

void AKPlayerCharacter::SetIsFacingRight(bool _IsFacingRight)
{
	bIsFacingRight = _IsFacingRight;
	if (bIsHighCover)
	{
		ToggleCameraPosition();
	}
}

void AKPlayerCharacter::SetAbleToFireFromCover(bool _AbleToFireFromCover)
{
	//UE_LOG(LogTemp, Warning, TEXT("AKPlayerCharacter::SetAbleToFire - _AbleToFire: %s"), _AbleToFire ? TEXT("true") : TEXT("false"));
	bAbleToFireFromCover = _AbleToFireFromCover;
	ShowHideCrosshairsEvent.Broadcast(bAbleToFireFromCover ? ESlateVisibility::Visible : ESlateVisibility::Hidden);
}

void AKPlayerCharacter::TakeDamage(float _DamageMin, float _DamageMax, float _CritDamageMultiplier, float _CritChance, FName HitBoneName, AActor* InstigatorActor)
{
	// ...
}

// Called to bind functionality to input
void AKPlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Movement Input
	PlayerInputComponent->BindAxis("MoveForward", this, &AKPlayerCharacter::InputAxis_MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AKPlayerCharacter::InputAxis_MoveRight);
	
	// Rotation Input
	PlayerInputComponent->BindAxis("MousePitch", this, &AKPlayerCharacter::InputAxis_MousePitch);
	PlayerInputComponent->BindAxis("MouseYaw", this, &AKPlayerCharacter::InputAxis_MouseYaw);
	
	// Input Action
	PlayerInputComponent->BindAction("SprintButton", IE_Pressed, this, &AKPlayerCharacter::InputAction_SprintButton);
	PlayerInputComponent->BindAction("CoverButton", IE_Pressed, this, &AKPlayerCharacter::InputAction_CoverButton);

	PlayerInputComponent->BindAction("AimButton", IE_Pressed, this, &AKPlayerCharacter::InputAction_AimButtonPressed);
	PlayerInputComponent->BindAction("AimButton", IE_Released, this, &AKPlayerCharacter::InputAction_AimButtonReleased);

	PlayerInputComponent->BindAction("FireButton", IE_Pressed, this, &AKPlayerCharacter::InputAction_FireButtonPressed);
	PlayerInputComponent->BindAction("FireButton", IE_Released, this, &AKPlayerCharacter::InputAction_FireButtonReleased);
	PlayerInputComponent->BindAction("ToggleCamera", IE_Pressed, this, &AKPlayerCharacter::InputAction_ToggleCamera);
}

// Set is taking cover, if it ends up true then set the actor rotation
void AKPlayerCharacter::SetIsTakingCover(bool _bIsTakingCover)
{
	// ensure we are not calling this function to set the same value, this might happen because of our Aim_Notifies
	if (IsTakingCover == _bIsTakingCover) { return; }

	IsTakingCover = _bIsTakingCover;
	bUseControllerRotationYaw = !_bIsTakingCover;
	if (IsTakingCover)
	{
		IsSprinting = false;
		SetActorRotation(CoverRotation);
		if (bIsHighCover)
		{
			ToggleCameraPosition();
		}
	}
	else
	{
		bIsAiming = false;
		if (!bCameraIsFacingRight)
		{
			ToggleCameraPosition();
		}
	}
	ShowHideCrosshairsEvent.Broadcast(IsTakingCover ? ESlateVisibility::Hidden : ESlateVisibility::Visible);
}

void AKPlayerCharacter::InputAxis_MoveForward(float AxisValue)
{
	ForwardAxis = AxisValue;

	// First get delta rotation between player and controller
	auto deltaRotator = UKismetMathLibrary::NormalizedDeltaRotator(GetControlRotation(), GetActorRotation());

	// Who came up with this is was on drugs, please don't blame me
	// You take the cosine from the DeltaYaw, multiply the AxisValue and add the sine of the (0.0f - DeltaYaw) multiplied by the right axis value
	auto cosdDeltaYaw = UKismetMathLibrary::DegCos(deltaRotator.Yaw);
	auto RightAxisDelta = UKismetMathLibrary::DegSin(0.0f - deltaRotator.Yaw) * RightAxis;
	
	MoveForward = (ForwardAxis * cosdDeltaYaw) + RightAxisDelta;
}

void AKPlayerCharacter::InputAxis_MoveRight(float AxisValue)
{
	RightAxis = AxisValue;

	// First get delta rotation between player and controller
	auto deltaRotator = UKismetMathLibrary::NormalizedDeltaRotator(GetControlRotation(), GetActorRotation());

	// Who came up with this is was on drugs, please don't blame me
	// You take the cosine from the DeltaYaw, multiply the AxisValue and add the sine of the (0.0f - DeltaYaw) multiplied by the right axis value
	auto cosdDeltaYaw = UKismetMathLibrary::DegCos(0.0f - deltaRotator.Yaw);
	auto ForwardAxisDelta = UKismetMathLibrary::DegSin(deltaRotator.Yaw) * ForwardAxis;

	MoveRight = (RightAxis * cosdDeltaYaw) + ForwardAxisDelta;
}

void AKPlayerCharacter::InputAxis_MousePitch(float AxisValue)
{
	float InvertMouseMultiplier = InvertMouse ? -1.0f : 1.0f;
	float PitchValue = (AxisValue * MouseSensitivity) * InvertMouseMultiplier;
	AddControllerPitchInput(PitchValue);
}

void AKPlayerCharacter::InputAxis_MouseYaw(float AxisValue)
{
	float InvertMouseMultiplier = InvertMouse ? -1.0f : 1.0f;
	float YawValue = (AxisValue * MouseSensitivity) * InvertMouseMultiplier;
	AddControllerYawInput(YawValue);
}

void AKPlayerCharacter::InputAction_SprintButton()
{
	if (MoveForward > 0)
	{
		IsSprinting = !IsSprinting;
	}
	ShowHideCrosshairsEvent.Broadcast(IsSprinting ? ESlateVisibility::Hidden : ESlateVisibility::Visible);
}

void AKPlayerCharacter::InputAction_CoverButton()
{
	TakeCover();
}

void AKPlayerCharacter::InputAction_AimButtonPressed()
{
	if (bIsHighCover)
	{
		bIsAiming = (EndofCoverLeft && !bIsFacingRight) || (EndofCoverRight && bIsFacingRight);
	}
	else
	{
		bIsAiming = true;
	}
	bool ableToZoom = (!IsTakingCover || bIsAiming) && !IsSprinting;
	SetFOV(ableToZoom);
}

void AKPlayerCharacter::InputAction_AimButtonReleased()
{
	bIsAiming = false;
	SetFOV(bIsAiming);
}

void AKPlayerCharacter::InputAction_FireButtonPressed()
{
	if ( (!IsTakingCover || (bIsAiming && bAbleToFireFromCover)) && !IsSprinting)
	{
		AKPlayerController* PC = Cast<AKPlayerController>(GetController());
		auto HitLocation = PC->CalculateHitLocation();

		// Fire with our equipped weapon
		Weapon->FireWeapon(HitLocation);
	}
}

void AKPlayerCharacter::InputAction_FireButtonReleased()
{
	//bIsFiring = false;
}

void AKPlayerCharacter::InputAction_ToggleCamera()
{
	if (!bCameraIsFacingRight)
	{
		// move right
		ToggleCameraTimeline->Reverse();
		bCameraIsFacingRight = true;
	}
	else if (bCameraIsFacingRight)
	{
		// move left
		ToggleCameraTimeline->Play();
		bCameraIsFacingRight = false;
	}
}

void AKPlayerCharacter::TakeCover()
{
	if (IsTakingCover)
	{
		SetIsTakingCover(false);
	}
	else
	{
		bool coverCheck = CoverAngleIsOk() && CoverAvailable;
		SetIsTakingCover(coverCheck);
	}
}

bool AKPlayerCharacter::CoverAngleIsOk()
{
	auto actorForward = GetActorRotation().Vector();
	auto coverForward = CoverRotation.Vector();
	// Get Delta rotation from dot product, since it always returns a positive angle
	auto dotResult = UKismetMathLibrary::Dot_VectorVector(actorForward, coverForward);
	auto angleBetweenVectors = UKismetMathLibrary::DegAcos(dotResult);
	
	return angleBetweenVectors < AngleToCover;
}

void AKPlayerCharacter::SetFOV(bool _isAiming)
{
	if (!ensure(ZoomCurve)) { return; }
	if (_isAiming)
	{
		ZoomTimeline->Play();
	}
	else
	{
		ZoomTimeline->Reverse();
	}
}

void AKPlayerCharacter::ZoomTimelineFloatReturn(float val)
{
	float lerpFOV = UKismetMathLibrary::Lerp(90, Weapon->GetZoomFOV(), val);
	FollowCamera->SetFieldOfView(lerpFOV);
}

void AKPlayerCharacter::ToggleCameraPosition()
{
	if (((bIsFacingRight || !IsTakingCover) && !bCameraIsFacingRight))
	{
		// move right
		ToggleCameraTimeline->Reverse();
		bCameraIsFacingRight = true;
	}
	else if (!bIsFacingRight && bCameraIsFacingRight)
	{
		// move left
		ToggleCameraTimeline->Play();
		bCameraIsFacingRight = false;
	}
}

void AKPlayerCharacter::ToggleCameraTimelineFloatReturn(float val)
{
	// slide the camera in the opposite direction offset
	CameraBoom->SocketOffset = FVector(socketOffset.X, socketOffset.Y * val, socketOffset.Z);
}
