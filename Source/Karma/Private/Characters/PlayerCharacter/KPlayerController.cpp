// Property of Erick Pombo Sonderblohm

#include "Karma.h"
#include "KPlayerController.h"

#define WEAPONCHANNEL        ECC_GameTraceChannel2

AKPlayerController::AKPlayerController()
{

}

FVector AKPlayerController::CalculateHitLocation()
{
	//UE_LOG(LogTemp, Warning, TEXT("CalculateHitLocation"));
	FVector HitLocation(0);
	GetSightRayHitLocation(HitLocation);
	return HitLocation;
}

void AKPlayerController::BeginPlay()
{
	Super::BeginPlay();
}

// Get world location of line trace through crosshair, true if hits landscape
bool AKPlayerController::GetSightRayHitLocation(FVector& HitLocation) const
{
	// Find the crosshair position
	int32 ViewportSizeX, ViewportSizeY;
	GetViewportSize(ViewportSizeX, ViewportSizeY);
	FVector2D ScreenLocation = FVector2D(ViewportSizeX * CrossHairXLocation, ViewportSizeY * CrossHairYLocation);

	// "De-project" the screen position of the crosshair to a world direction
	FVector LookDirection(0);
	if (GetLookDirection(ScreenLocation, LookDirection))
	{
		// Line-trace along that look direction, and see what we hit (up to max range)
		return GetLookVectorHitLocation(LookDirection, HitLocation);
	}
	return false;
}

bool AKPlayerController::GetLookDirection(FVector2D ScreenLocation, FVector& LookDirection) const
{
	FVector CameraWorldlocation(0);	// To be discarded
	return DeprojectScreenPositionToWorld(
		ScreenLocation.X,
		ScreenLocation.Y,
		CameraWorldlocation,
		LookDirection
	);
}

bool AKPlayerController::GetLookVectorHitLocation(FVector LookDirection, FVector& HitLocation) const
{
	FHitResult HitResult;
	FVector StartLocation = PlayerCameraManager->GetCameraLocation();
	FVector EndLocation = StartLocation + (LookDirection * LineTraceRange);

	// using custom trace channels on C++
	// look into DefaultEngine.ini and search for your custom trace/channel
	// you can #define the channel name to make it easier to access

	if (GetWorld()->LineTraceSingleByChannel(
		HitResult,
		StartLocation,
		EndLocation,
		ECollisionChannel::WEAPONCHANNEL
	))
	{
		HitLocation = HitResult.Location;
		return true;
	}
	HitLocation = EndLocation;
	return false;
}
