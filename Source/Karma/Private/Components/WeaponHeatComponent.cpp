// Property of Erick Pombo Sonderblohm

#include "Karma.h"
#include "WeaponHeatComponent.h"


// Sets default values for this component's properties
UWeaponHeatComponent::UWeaponHeatComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UWeaponHeatComponent::BeginPlay()
{
	Super::BeginPlay();
	
}


// Called every frame
void UWeaponHeatComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	ReduceHeatBuildUp(DeltaTime);
	OverHeatAlarm();
}

void UWeaponHeatComponent::FireHeatBuildUp()
{
	HeatCurrent += HeatBuildUpRate;
	if (HeatCurrent >= HeatMax)
	{
		HeatCurrent = HeatMax;
		bIsWeaponOverheated = true;
		WeaponOverheatEvent.Broadcast(bIsWeaponOverheated);
	}
}

bool UWeaponHeatComponent::GetIsOverHeated() const
{
	return bIsWeaponOverheated;
}

float UWeaponHeatComponent::GetHeatPercent() const
{
	return HeatCurrent / HeatMax;
}

void UWeaponHeatComponent::ReduceHeatBuildUp(float DeltaT)
{
	if (HeatCurrent > 0.0f)
	{
		HeatCurrent -= DeltaT * HeatReductionRate;

		if (HeatCurrent <= 0)
		{
			HeatCurrent = 0.0f;
			bIsWeaponOverheated = false;
			ClearOverheatTimerHadle();

			// Broadcast overheated information
			WeaponOverheatEvent.Broadcast(bIsWeaponOverheated);
		}

		// Broadcast our overheat percent
		WeaponOverheatPercent.Broadcast(HeatCurrent / HeatMax);
	}
}

void UWeaponHeatComponent::OverHeatAlarm()
{
	if (bIsWeaponOverheated)
	{
		//  Do once
		if (!bOverHeatAlarmExecuted)
		{
			bOverHeatAlarmExecuted = true;

			// Delay will play sound every 0.5 secs
			GetWorld()->GetTimerManager().SetTimer(DelayHandle, this, &UWeaponHeatComponent::PlayOverHeatSound, 0.5f, true, 0.0f);
		}
	}
}

void UWeaponHeatComponent::PlayOverHeatSound()
{
	if (!ensure(WeaponOverheatSound)) { return; }
	UGameplayStatics::PlaySound2D(this, WeaponOverheatSound);
}

void UWeaponHeatComponent::ClearOverheatTimerHadle()
{
	bOverHeatAlarmExecuted = false;
	GetWorld()->GetTimerManager().ClearTimer(DelayHandle);
}

