// Property of Erick Pombo Sonderblohm

#include "Karma.h"
#include "WeaponFireComponent.h"
#include "KPlayerController.h"
#include "BaseWeapon.h"
#include "Kismet/KismetMathLibrary.h"
#include "BaseProjectile.h"
#include "KPlayerCharacter.h"

// Sets default values for this component's properties
UWeaponFireComponent::UWeaponFireComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}

// Called when the game starts
void UWeaponFireComponent::BeginPlay()
{
	Super::BeginPlay();

	Weapon = Cast<ABaseWeapon>(GetOwner());
}

// Called every frame
void UWeaponFireComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UWeaponFireComponent::Fire(FVector HitLocation)
{
	//UE_LOG(LogTemp, Warning, TEXT("UWeaponFireComponent::Fire"));

	if (!ensure(Weapon)) { return; }
	USkeletalMeshComponent* WeaponMesh = Weapon->FindComponentByClass<USkeletalMeshComponent>();
	//const UStaticMeshSocket* WeaponSocket = Weapon->GetSocketByName(FName("Muzzle"));

	// get actor location and rotation for the projectile to spawn
	auto MuzzleLocation = WeaponMesh->GetSocketLocation(FName("Muzzle"));
	auto rotation = UKismetMathLibrary::FindLookAtRotation(MuzzleLocation, HitLocation);

	/*DrawDebugLine(
		GetWorld(),
		MuzzleLocation,
		HitLocation,
		FColor(0, 255, 120),
		false, 3.0f, 0,
		1.0f
	);*/

	// Fire Animation
	if (FireAnimation != nullptr)
	{
		WeaponMesh->PlayAnimation(FireAnimation, false);
	}

	// Spawn Projectile
	if (!ensure(ProjectileBlueprint)) { return; }
	if (ProjectileBlueprint != nullptr)
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.Instigator = Weapon->GetInstigator();
		ABaseProjectile* Projectile = GetWorld()->SpawnActor<ABaseProjectile>(ProjectileBlueprint->GetOwnerClass(), MuzzleLocation, rotation, SpawnParams);
		Projectile->LaunchProjectile(LaunchSpeed);
		Projectile->SetDamageInfo(DamageMin, DamageMax, CritDamageMultiplier, CritChance);

		//Ammo = Ammo > 0 ? Ammo - 1 : 0;
	}
}

