// Property of Erick Pombo Sonderblohm

#include "Karma.h"
#include "WeaponRecoilComponent.h"
#include "Kismet/KismetMathLibrary.h"


// Sets default values for this component's properties
UWeaponRecoilComponent::UWeaponRecoilComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	RecoilTimeline = CreateDefaultSubobject<UTimelineComponent>(TEXT("RecoilTimeline"));

	//Bind the Callback function for the float return value
	InterpFunction.BindUFunction(this, FName{ TEXT("TimelineFloatReturn") });
}


// Called when the game starts
void UWeaponRecoilComponent::BeginPlay()
{
	Super::BeginPlay();

	//Add the float curve to the timeline and connect it to your my interpolation function
	RecoilTimeline->AddInterpFloat(RecoilCurve, InterpFunction, FName{ TEXT("RecoilInterp") });
	
}


// Called every frame
void UWeaponRecoilComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UWeaponRecoilComponent::ApplyRecoil()
{
	if (!ensure(RecoilCurve)) { return; }
	RecoilTimeline->PlayFromStart();
}

void UWeaponRecoilComponent::TimelineFloatReturn(float val)
{
	// Add Pitch and Yaw to our PlayerController to simulate the recoil based on our Curve float
	float vRecoil = VerticalRecoil * -1;
	float vLerpVal = UKismetMathLibrary::Lerp(0.0f, vRecoil, val);
	float hLerpVal = UKismetMathLibrary::Lerp(0.0f, HorizontalRecoil, val);

	auto PC = GetOwner()->GetWorld()->GetFirstPlayerController();
	if (PC)
	{
		PC->AddPitchInput(vLerpVal);
		PC->AddYawInput(hLerpVal);
	}
}

