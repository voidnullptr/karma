// Property of Erick Pombo Sonderblohm

#include "Karma.h"
#include "KAIController.h"

/* Behavior Tree */
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"



AKAIController::AKAIController()
{
	BehaviorComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorComp"));
	BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComp"));
}

void AKAIController::Possess(APawn* InPawn)
{
	Super::Possess(InPawn);
	SetupBlackBoard(InPawn);
}

void AKAIController::BeginPlay()
{
	Super::BeginPlay();
}

void AKAIController::SetupBlackBoard(APawn* InPawn)
{
	if (BehaviorTree)
	{
		if (BehaviorTree->BlackboardAsset)
		{
			BlackboardComp->InitializeBlackboard(*BehaviorTree->BlackboardAsset);
			BlackboardComp->SetValueAsObject(SelfActorKeyName, InPawn);
		}

		// Start the behavior tree
		BehaviorComp->StartTree(*BehaviorTree);
	}
}
