// Property of Erick Pombo Sonderblohm

#include "Karma.h"
#include "UpdateCombatStatus.h"
#include "BaseEnemyCharacter.h"
#include "AIController.h"
#include "KPlayerCharacter.h"

/* Behavior Tree */
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"



void UUpdateCombatStatus::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
// 	UE_LOG(LogTemp, Warning, TEXT("UUpdateCombatStatus::TickNode"));

	AAIController* KAIC = OwnerComp.GetAIOwner();
	auto EnemyChar = Cast<ABaseEnemyCharacter>(KAIC->GetPawn());
	if (EnemyChar)
	{
		// Get a pointer to our BlackBoard comp
		UBlackboardComponent* BlackboardComp = OwnerComp.GetBlackboardComponent();

		auto BBCombatStatus = BlackboardComp->GetValueAsEnum(CombatStatusKeySelector.SelectedKeyName);
		if (static_cast<uint8>(EnemyChar->CombatStatus) != BBCombatStatus)
		{
			// Update the CombatStatus from the BlackBoard, if the combat status is different
			BlackboardComp->SetValueAsEnum(CombatStatusKeySelector.SelectedKeyName, static_cast<uint8>(EnemyChar->CombatStatus));
		}

		// Update the PlayerCharacter, just once since it's a single player game
		auto PlayerCharacter = EnemyChar->PlayerCharacter;
		if (PlayerCharacter && !bPlayerCharacterHasBeenSet)
		{
			BlackboardComp->SetValueAsObject(PlayerCharacterKeySelector.SelectedKeyName, PlayerCharacter);
			bPlayerCharacterHasBeenSet = true;
		}

		// Update Combat Type, only once
		if (!bIsCombatTypeUpdated)
		{
			BlackboardComp->SetValueAsEnum(CombatTypeKeySelector.SelectedKeyName, static_cast<uint8>(EnemyChar->CombatType));
			bIsCombatTypeUpdated = true;
		}
	}
}
