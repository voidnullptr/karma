// Property of Erick Pombo Sonderblohm

#include "Karma.h"
#include "SetMoveSpeed.h"
#include "AIController.h"
#include "BaseEnemyCharacter.h"



EBTNodeResult::Type USetMoveSpeed::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AAIController* ownedActorController = OwnerComp.GetAIOwner();
	ABaseEnemyCharacter* EnemyAI = Cast<ABaseEnemyCharacter>(ownedActorController->GetPawn());

	if (EnemyAI)
	{
		UCharacterMovementComponent* MovementComponent = EnemyAI->GetCharacterMovement();
		if (MovementComponent)
		{
			MovementComponent->MaxWalkSpeed = MaxWalkSpeed;
			return EBTNodeResult::Succeeded;
		}
	}
	return EBTNodeResult::InProgress;
}
