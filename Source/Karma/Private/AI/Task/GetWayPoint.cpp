// Property of Erick Pombo Sonderblohm

#include "Karma.h"
#include "GetWayPoint.h"
#include "AIController.h"
#include "BaseEnemyCharacter.h"
#include "BehaviorTree/BlackboardComponent.h"


EBTNodeResult::Type UGetWayPoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);

	if (!bWaypointsSet)
	{
		AAIController* ownedActorController = OwnerComp.GetAIOwner();
		ABaseEnemyCharacter* EnemyAI = Cast<ABaseEnemyCharacter>(ownedActorController->GetPawn());

		if (EnemyAI)
		{
			UBlackboardComponent* BlackboardComp = OwnerComp.GetBlackboardComponent();
			BlackboardComp->SetValueAsVector(Waypoint1KeySelector.SelectedKeyName, EnemyAI->Waypoint_1->GetComponentLocation());
			BlackboardComp->SetValueAsVector(Waypoint2KeySelector.SelectedKeyName, EnemyAI->Waypoint_2->GetComponentLocation());
			bWaypointsSet = true;
		}
	}
	return EBTNodeResult::Succeeded;
}
