// Property of Erick Pombo Sonderblohm

#include "Karma.h"
#include "BaseCoverObject.h"
#include "CoverInterface.h"

// Sets default values
ABaseCoverObject::ABaseCoverObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	DefaultSceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultSceneRoot"));
	this->SetRootComponent(DefaultSceneRoot);

	CoverMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CoverMesh"));
	CoverMesh->AttachToComponent(DefaultSceneRoot, FAttachmentTransformRules::KeepRelativeTransform);

	// Front cover box
	MainCoverBoxFront = CreateDefaultSubobject<UBoxComponent>(TEXT("MainCoverBoxFront"));
	MainCoverBoxFront->AttachToComponent(DefaultSceneRoot, FAttachmentTransformRules::KeepRelativeTransform);

	ArrowFront = CreateDefaultSubobject<UArrowComponent>(TEXT("ArrowFront"));
	ArrowFront->AttachToComponent(MainCoverBoxFront, FAttachmentTransformRules::KeepRelativeTransform);

	EndOfCoverFrontLeft = CreateDefaultSubobject<UBoxComponent>(TEXT("EndOfCoverFrontLeft"));
	EndOfCoverFrontLeft->AttachToComponent(MainCoverBoxFront, FAttachmentTransformRules::KeepRelativeTransform);

	EndOfCoverFrontRight = CreateDefaultSubobject<UBoxComponent>(TEXT("EndOfCoverFrontRight"));
	EndOfCoverFrontRight->AttachToComponent(MainCoverBoxFront, FAttachmentTransformRules::KeepRelativeTransform);

	// Back cover box
	MainCoverBoxBack = CreateDefaultSubobject<UBoxComponent>(TEXT("MainCoverBoxBack"));
	MainCoverBoxBack->AttachToComponent(DefaultSceneRoot, FAttachmentTransformRules::KeepRelativeTransform);

	ArrowBack = CreateDefaultSubobject<UArrowComponent>(TEXT("ArrowBack"));
	ArrowBack->AttachToComponent(MainCoverBoxBack, FAttachmentTransformRules::KeepRelativeTransform);

	EndOfCoverBackLeft = CreateDefaultSubobject<UBoxComponent>(TEXT("EndOfCoverBackLeft"));
	EndOfCoverBackLeft->AttachToComponent(MainCoverBoxBack, FAttachmentTransformRules::KeepRelativeTransform);

	EndOfCoverBackRight = CreateDefaultSubobject<UBoxComponent>(TEXT("EndOfCoverBackRight"));
	EndOfCoverBackRight->AttachToComponent(MainCoverBoxBack, FAttachmentTransformRules::KeepRelativeTransform);
}

// Called when the game starts or when spawned
void ABaseCoverObject::BeginPlay()
{
	Super::BeginPlay();
	
	// Bind Overlap Events

	//Main Cover boxes
	MainCoverBoxFront->OnComponentBeginOverlap.AddDynamic(this, &ABaseCoverObject::OnComponentBeginOverLap_MainCoverBoxFront);
	MainCoverBoxFront->OnComponentEndOverlap.AddDynamic(this, &ABaseCoverObject::OnComponentEndOverLap_MainCoverBox);
	MainCoverBoxBack->OnComponentBeginOverlap.AddDynamic(this, &ABaseCoverObject::OnComponentBeginOverLap_MainCoverBoxBack);
	MainCoverBoxBack->OnComponentEndOverlap.AddDynamic(this, &ABaseCoverObject::OnComponentEndOverLap_MainCoverBox);

	// End Left
	EndOfCoverFrontLeft->OnComponentBeginOverlap.AddDynamic(this, &ABaseCoverObject::OnComponentBeginOverLap_EndOfCoverLeft);
	EndOfCoverFrontLeft->OnComponentEndOverlap.AddDynamic(this, &ABaseCoverObject::OnComponentEndOverLap_EndOfCoverLeft);
	EndOfCoverBackLeft->OnComponentBeginOverlap.AddDynamic(this, &ABaseCoverObject::OnComponentBeginOverLap_EndOfCoverLeft);
	EndOfCoverBackLeft->OnComponentEndOverlap.AddDynamic(this, &ABaseCoverObject::OnComponentEndOverLap_EndOfCoverLeft);

	// End Right
	EndOfCoverFrontRight->OnComponentBeginOverlap.AddDynamic(this, &ABaseCoverObject::OnComponentBeginOverLap_EndOfCoverRight);
	EndOfCoverFrontRight->OnComponentEndOverlap.AddDynamic(this, &ABaseCoverObject::OnComponentEndOverLap_EndOfCoverRight);
	EndOfCoverBackRight->OnComponentBeginOverlap.AddDynamic(this, &ABaseCoverObject::OnComponentBeginOverLap_EndOfCoverRight);
	EndOfCoverBackRight->OnComponentEndOverlap.AddDynamic(this, &ABaseCoverObject::OnComponentEndOverLap_EndOfCoverRight);
}

// Called every frame
void ABaseCoverObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABaseCoverObject::OnComponentBeginOverLap_MainCoverBoxFront(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// Other Actor is the actor that triggered the event. Check that is not ourself.  
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		// Check that overlapped actor implements the Cover Interface
		ICoverInterface* ReactingObject = Cast<ICoverInterface>(OtherActor); // ReactingObject will be non-null if OriginalObject implements UReactToTriggerInterface.
		if (ReactingObject)
		{
			ReactingObject->OnCoverAvailable(true, bIsHighCover, ArrowFront->GetComponentRotation());
		}
	}
}

void ABaseCoverObject::OnComponentBeginOverLap_MainCoverBoxBack(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// Other Actor is the actor that triggered the event. Check that is not ourself.  
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		// Check that overlapped actor implements the Cover Interface
		ICoverInterface* ReactingObject = Cast<ICoverInterface>(OtherActor); // ReactingObject will be non-null if OriginalObject implements UReactToTriggerInterface.
		if (ReactingObject)
		{
			ReactingObject->OnCoverAvailable(true, bIsHighCover, ArrowBack->GetComponentRotation());
		}
	}
}

void ABaseCoverObject::OnComponentEndOverLap_MainCoverBox(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	// Other Actor is the actor that triggered the event. Check that is not ourself.  
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		// Check that overlapped actor implements the Cover Interface
		ICoverInterface* ReactingObject = Cast<ICoverInterface>(OtherActor); // ReactingObject will be non-null if OriginalObject implements UReactToTriggerInterface.
		if (ReactingObject)
		{
			ReactingObject->OnCoverAvailable(false, false, FRotator(ForceInitToZero));
		}
	}
}

void ABaseCoverObject::OnComponentBeginOverLap_EndOfCoverLeft(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// Other Actor is the actor that triggered the event. Check that is not ourself.  
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		// Check that overlapped actor implements the Cover Interface
		ICoverInterface* ReactingObject = Cast<ICoverInterface>(OtherActor); // ReactingObject will be non-null if OriginalObject implements UReactToTriggerInterface.
		if (ReactingObject)
		{
			ReactingObject->EndCover(true, false);
		}
	}
}

void ABaseCoverObject::OnComponentEndOverLap_EndOfCoverLeft(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	// Other Actor is the actor that triggered the event. Check that is not ourself.  
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		// Check that overlapped actor implements the Cover Interface
		ICoverInterface* ReactingObject = Cast<ICoverInterface>(OtherActor); // ReactingObject will be non-null if OriginalObject implements UReactToTriggerInterface.
		if (ReactingObject)
		{
			ReactingObject->EndCover(false, false);
		}
	}
}

void ABaseCoverObject::OnComponentBeginOverLap_EndOfCoverRight(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// Other Actor is the actor that triggered the event. Check that is not ourself.  
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		// Check that overlapped actor implements the Cover Interface
		ICoverInterface* ReactingObject = Cast<ICoverInterface>(OtherActor); // ReactingObject will be non-null if OriginalObject implements UReactToTriggerInterface.
		if (ReactingObject)
		{
			ReactingObject->EndCover(false, true);
		}
	}
}

void ABaseCoverObject::OnComponentEndOverLap_EndOfCoverRight(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	// Other Actor is the actor that triggered the event. Check that is not ourself.  
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		// Check that overlapped actor implements the Cover Interface
		ICoverInterface* ReactingObject = Cast<ICoverInterface>(OtherActor); // ReactingObject will be non-null if OriginalObject implements UReactToTriggerInterface.
		if (ReactingObject)
		{
			ReactingObject->EndCover(false, false);
		}
	}
}
