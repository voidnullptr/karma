// Property of Erick Pombo Sonderblohm

#pragma once

#include "GameFramework/Actor.h"
#include "BaseCoverObject.generated.h"

class ICoverInterface;

UCLASS()
class KARMA_API ABaseCoverObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseCoverObject();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Main cover boxes overlap events
	UFUNCTION()
	void OnComponentBeginOverLap_MainCoverBoxFront(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnComponentBeginOverLap_MainCoverBoxBack(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnComponentEndOverLap_MainCoverBox(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	// End Overlap events
	UFUNCTION()
	void OnComponentBeginOverLap_EndOfCoverLeft(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnComponentEndOverLap_EndOfCoverLeft(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	// End Overlap events
	UFUNCTION()
	void OnComponentBeginOverLap_EndOfCoverRight(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnComponentEndOverLap_EndOfCoverRight(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Cover")
	bool bIsHighCover = false;

	// Root Component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Root")
	USceneComponent* DefaultSceneRoot;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Mesh")
	UStaticMeshComponent* CoverMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Overlap Component")
	UBoxComponent* MainCoverBoxFront;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Overlap Component")
	UBoxComponent* MainCoverBoxBack;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Overlap Component")
	UBoxComponent* EndOfCoverFrontLeft;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Overlap Component")
	UBoxComponent* EndOfCoverFrontRight;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Overlap Component")
	UBoxComponent* EndOfCoverBackLeft;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Overlap Component")
	UBoxComponent* EndOfCoverBackRight;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Arrow Component")
	UArrowComponent* ArrowFront;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Arrow Component")
	UArrowComponent* ArrowBack;
	
};
