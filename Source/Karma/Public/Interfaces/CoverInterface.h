// Property of Erick Pombo Sonderblohm

#pragma once

#include "CoverInterface.generated.h"

UINTERFACE(Blueprintable)
class UCoverInterface : public UInterface
{
	GENERATED_BODY()
};

class ICoverInterface
{
	GENERATED_BODY()

public:

	UFUNCTION(Category = "Trigger Reaction")
	virtual void OnCoverAvailable(bool _bIsCoverAvailable, bool _bIsHighCover, FRotator _CoverRotation){}

	UFUNCTION(Category = "Trigger Reaction")
	virtual void EndCover(bool _bEndCoverLeft, bool _bEndCoverRight){}
};