// Property of Erick Pombo Sonderblohm

#pragma once

#include "DamageInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UDamageInterface : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

/**
 * 
 */
class KARMA_API IDamageInterface
{
	GENERATED_IINTERFACE_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	UFUNCTION(Category = "Weapon Damage")
	virtual void TakeDamage(float _DamageMin, float _DamageMax, float _CritDamageMultiplier, float _CritChance, FName HitBoneName, AActor* InstigatorActor) {}
};
