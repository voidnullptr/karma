// Property of Erick Pombo Sonderblohm

#pragma once

#include "BehaviorTree/BTService.h"
#include "UpdateCombatStatus.generated.h"

struct FBlackboardKeySelector;

/**
 * 
 */
UCLASS()
class KARMA_API UUpdateCombatStatus : public UBTService
{
	GENERATED_BODY()

protected:
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

public:

	UPROPERTY(EditAnywhere, Category = "AIBlackboard")
	FBlackboardKeySelector CombatStatusKeySelector;

	UPROPERTY(EditAnywhere, Category = "AIBlackboard")
	FBlackboardKeySelector PlayerCharacterKeySelector;

	UPROPERTY(EditAnywhere, Category = "AIBlackboard")
	FBlackboardKeySelector CombatTypeKeySelector;

private:

	bool bIsCombatTypeUpdated = false;
	bool bPlayerCharacterHasBeenSet = false;

};
