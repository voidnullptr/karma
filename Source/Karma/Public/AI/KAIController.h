// Property of Erick Pombo Sonderblohm

#pragma once

#include "AIController.h"
#include "KAIController.generated.h"

class UBehaviorTreeComponent;
class UBlackboardComponent;

/**
 * 
 */
UCLASS()
class KARMA_API AKAIController : public AAIController
{
	GENERATED_BODY()

public:

	AKAIController();


	virtual void Possess(APawn* InPawn) override;

protected:
	virtual void BeginPlay() override;

private:

	UBehaviorTreeComponent* BehaviorComp;
	UBlackboardComponent* BlackboardComp;

	UPROPERTY(EditDefaultsOnly, Category = "AIBehaviorTree")
	UBehaviorTree* BehaviorTree;

	UPROPERTY(EditDefaultsOnly, Category = "AIBehaviorTree")
	FName SelfActorKeyName = "SelfActor";

	UPROPERTY(EditDefaultsOnly, Category = "AIBehaviorTree")
	FName PlayerKeyName = "PlayerCharacter";

	UPROPERTY(EditDefaultsOnly, Category = "AIBehaviorTree")
	FName CombatStatusKeyName = "CombatStatus";

	UPROPERTY(EditDefaultsOnly, Category = "AIBehaviorTree")
	FName CombatTypeKeyName = "CombatType";

	void SetupBlackBoard(APawn* InPawn);

};
