// Property of Erick Pombo Sonderblohm

#pragma once

#include "BehaviorTree/BTTaskNode.h"
#include "SetMoveSpeed.generated.h"

/**
 * 
 */
UCLASS()
class KARMA_API USetMoveSpeed : public UBTTaskNode
{
	GENERATED_BODY()
	
	
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

public:

	UPROPERTY(EditAnywhere, Category = "Movement")
	float MaxWalkSpeed = 300.0f;
	
};
