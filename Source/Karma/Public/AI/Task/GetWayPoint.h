// Property of Erick Pombo Sonderblohm

#pragma once

#include "BehaviorTree/BTTaskNode.h"
#include "GetWayPoint.generated.h"

/**
 * 
 */
UCLASS()
class KARMA_API UGetWayPoint : public UBTTaskNode
{
	GENERATED_BODY()
	
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

public:

	UPROPERTY(EditAnywhere, Category = "AIBlackboard")
	FBlackboardKeySelector Waypoint1KeySelector;

	UPROPERTY(EditAnywhere, Category = "AIBlackboard")
	FBlackboardKeySelector Waypoint2KeySelector;

private:

	bool bWaypointsSet = false;

};
