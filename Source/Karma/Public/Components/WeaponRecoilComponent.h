// Property of Erick Pombo Sonderblohm

#pragma once

#include "Components/ActorComponent.h"
#include "WeaponRecoilComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class KARMA_API UWeaponRecoilComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UWeaponRecoilComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void ApplyRecoil();

private:

	UPROPERTY()
	UTimelineComponent* RecoilTimeline;

	UPROPERTY(EditDefaultsOnly, Category = "Recoil")
	UCurveFloat* RecoilCurve = nullptr;

	FOnTimelineFloat InterpFunction;

	UFUNCTION()
	void TimelineFloatReturn(float val);

	UPROPERTY(EditDefaultsOnly, Category = "Recoil", meta = (ClampMin = 0, ClampMax = 10, UIMin = 0, UIMax = 10))
	float HorizontalRecoil = 0.01f;

	UPROPERTY(EditDefaultsOnly, Category = "Recoil", meta = (ClampMin = 0, ClampMax = 10, UIMin = 0, UIMax = 10))
	float VerticalRecoil = 0.05f;
	
};
