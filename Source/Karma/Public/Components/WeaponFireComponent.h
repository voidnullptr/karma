// Property of Erick Pombo Sonderblohm

#pragma once

#include "Components/ActorComponent.h"
#include "WeaponFireComponent.generated.h"

// Enum for weapon state
UENUM()
enum class EFiringStatus : uint8
{
	Reloading,
	Idle,
	Aiming,
	OutOfAmmo
};

class ABaseProjectile;
class ABaseWeapon;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class KARMA_API UWeaponFireComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UWeaponFireComponent();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "Firing")
	void Fire(FVector HitLocation);

	UFUNCTION(BlueprintCallable, Category = "Ammo")
	int32 GetAmmo() const { return Ammo; }

	EFiringStatus GetFiringStatus() const { return FiringStatus; }

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadOnly, Category = "Reloading")
	EFiringStatus FiringStatus = EFiringStatus::Reloading;

	UPROPERTY(EditDefaultsOnly, Category = "Ammo")
	int32 Ammo = 100;

	UPROPERTY(EditDefaultsOnly, Category = "Animation")
	UAnimationAsset* FireAnimation = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	TSubclassOf<ABaseProjectile> ProjectileBlueprint;

private:
	
	ABaseWeapon* Weapon = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Damage")
	float DamageMin = 8.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Damage")
	float DamageMax = 10.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Damage")
	float CritDamageMultiplier = 2.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Damage")
	float CritChance = 10.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Firing")
	float LaunchSpeed = 4000;
	
};
