// Property of Erick Pombo Sonderblohm

#pragma once

#include "Components/ActorComponent.h"
#include "WeaponHeatComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FWeaponOverHeatDelegate, bool, WeaponOverheatEvent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FWeaponOverHeatPercentDelegate, float, WeaponOverheatPercent);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class KARMA_API UWeaponHeatComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UWeaponHeatComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintAssignable, Category = "Heat")
	FWeaponOverHeatDelegate WeaponOverheatEvent;

	UPROPERTY(BlueprintAssignable, Category = "Heat")
	FWeaponOverHeatPercentDelegate WeaponOverheatPercent;

	void FireHeatBuildUp();
	bool GetIsOverHeated() const;
	float GetHeatPercent() const;

private:

	UPROPERTY(VisibleAnywhere, Category = "Heat")
	bool bIsWeaponOverheated = false;

	UPROPERTY(VisibleAnywhere, Category = "Heat")
	float HeatCurrent = 0.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Heat")
	float HeatMax = 15.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Heat")
	float HeatReductionRate = 3.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Heat")
	float HeatBuildUpRate = 1.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Heat")
	USoundCue* WeaponOverheatSound = nullptr;

	FTimerHandle DelayHandle;
	bool bOverHeatAlarmExecuted = false;

	void ReduceHeatBuildUp(float DeltaT);
	void OverHeatAlarm();
	void ClearOverheatTimerHadle();

	UFUNCTION()
	void PlayOverHeatSound();
};
