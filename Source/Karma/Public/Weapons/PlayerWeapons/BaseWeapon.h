// Property of Erick Pombo Sonderblohm

#pragma once

#include "GameFramework/Actor.h"
#include "BaseWeapon.generated.h"

class UWeaponFireComponent;
class UWeaponHeatComponent;
class UWeaponRecoilComponent;

UCLASS()
class KARMA_API ABaseWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseWeapon();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void FireWeapon(FVector HitLocation);
	UWeaponFireComponent* GetFireComponent() const;
	UWeaponHeatComponent* GetHeatComponent() const;
	UWeaponRecoilComponent* GetRecoilComponent() const;
	float GetZoomFOV() const;

private:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character", meta = (AllowPrivateAccess = "true"))
	USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(EditDefaultsOnly, Category = "Zoom")
	float ZoomFOV = 45.0f;
};
