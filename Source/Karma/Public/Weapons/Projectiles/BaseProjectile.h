// Property of Erick Pombo Sonderblohm

#pragma once

#include "GameFramework/Actor.h"
#include "DamageInterface.h"
#include "BaseProjectile.generated.h"

class USphereComponent;
class UProjectileMovementComponent;
class UParticleSystemComponent;
class URadialForceComponent;
class USoundBase;
class UParticleSystem;

UCLASS()
class KARMA_API ABaseProjectile : public AActor, public IDamageInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void LaunchProjectile(float Speed);
	void SetDamageInfo(float _DamageMin, float _DamageMax, float _CritDamageMultiplier, float _CritChance);

private:

	UPROPERTY(VisibleAnywhere, Category = "Damage")
	float DamageMin = 0.0f;

	UPROPERTY(VisibleAnywhere, Category = "Damage")
	float DamageMax = 0.0f;

	UPROPERTY(VisibleAnywhere, Category = "Damage")
	float CritDamageMultiplier = 0.0f;

	UPROPERTY(VisibleAnywhere, Category = "Damage")
	float CritChance = 0.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float TimeToDieInSecs = 10.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float ProjectileDamage = 5.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	UParticleSystem* ImpactBlast = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	USoundBase* ImpactSound = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Initial Components")
	USphereComponent* Bullet = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Setup")
	UParticleSystemComponent* BlasterRay = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Setup")
	URadialForceComponent* ExplosionForce = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Setup")
	UProjectileMovementComponent* ProjectileMovement = nullptr;

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);

};
