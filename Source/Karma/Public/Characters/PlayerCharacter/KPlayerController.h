// Property of Erick Pombo Sonderblohm

#pragma once

#include "GameFramework/PlayerController.h"
#include "KPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class KARMA_API AKPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:

	AKPlayerController();

	UFUNCTION(BlueprintCallable, Category = "UI")
	FVector CalculateHitLocation();

protected:

	virtual void BeginPlay() override;

private:

	UPROPERTY(EditDefaultsOnly, Category = "UI")
	float CrossHairXLocation = 0.5f;

	UPROPERTY(EditDefaultsOnly, Category = "UI")
	float CrossHairYLocation = 0.5f; // 0.33333f;

	UPROPERTY(EditDefaultsOnly, Category = "LineTraceSingleByChannel")
	int32 LineTraceRange = 100000;	// 1 km -> 100000 cm

	bool GetSightRayHitLocation(FVector& HitLocation) const;
	bool GetLookDirection(FVector2D ScreenLocation, FVector& LookDirection) const;
	bool GetLookVectorHitLocation(FVector LookDirection, FVector& HitLocation) const;

};
