// Property of Erick Pombo Sonderblohm

#pragma once

#include "GameFramework/Character.h"
#include "CoverInterface.h"
#include "DamageInterface.h"
#include "KPlayerCharacter.generated.h"

class USpringArmComponent;
class UCameraComponent;
class ABaseWeapon;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FShowHideCrossHairDelegate, ESlateVisibility, CrossHairsVisibility);

UCLASS()
class KARMA_API AKPlayerCharacter : public ACharacter, public ICoverInterface, public IDamageInterface
{
	GENERATED_BODY()

private:

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FollowCamera;

	// Movement Axis Values
	float ForwardAxis = 0.0f;
	float RightAxis = 0.0f;

public:
	// Sets default values for this character's properties
	AKPlayerCharacter();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** Returns CameraBoom subobject **/
	FORCEINLINE USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	UPROPERTY(BlueprintAssignable, Category = "Crosshair")
	FShowHideCrossHairDelegate ShowHideCrosshairsEvent;

	virtual void OnCoverAvailable(bool _bIsCoverAvailable, bool _bIsHighCover, FRotator _CoverRotation);
	virtual void EndCover(bool _bEndCoverLeft, bool _bEndCoverRight);

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void EquipWeapon(TSubclassOf<ABaseWeapon> _WeaponToSpawnBP);

	ABaseWeapon* GetWeapon() const;

	UFUNCTION(BlueprintCallable, Category = "Movement")
	void SetIsFacingRight(bool _IsFacingRight);

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void SetAbleToFireFromCover(bool _AbleToFireFromCover);

	virtual void TakeDamage(float _DamageMin, float _DamageMax, float _CritDamageMultiplier, float _CritChance, FName HitBoneName, AActor* InstigatorActor) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Setup")
	TSubclassOf<ABaseWeapon> WeaponToSpawnBP;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	FName WeaponSocket = "WeaponSocket";

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Weapon")
	ABaseWeapon* Weapon = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	bool bAbleToFireFromCover = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Cover")
	bool bIsFacingRight = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Cover")
	float AngleToCover = 55.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Cover")
	bool CoverAvailable = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Cover")
	bool IsTakingCover = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Cover")
	bool EndofCoverLeft = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Cover")
	bool EndofCoverRight = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Cover")
	FRotator CoverRotation = FRotator(EForceInit::ForceInitToZero);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input")
	bool InvertMouse = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Input")
	bool IsSprinting = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Input")
	float MouseSensitivity = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Input")
	float MoveForward = 0.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Input")
	float MoveRight = 0.0f;

	UPROPERTY(BlueprintReadOnly, Category = "Input")
	bool bIsHighCover = false;

	UPROPERTY(BlueprintReadOnly, Category = "Input")
	bool bIsAiming = false;

	UPROPERTY(BlueprintReadOnly, Category = "Input")
	bool bIsFiring = false;

	UFUNCTION(BlueprintCallable, Category = "Cover")
	void SetIsTakingCover(bool _bIsTakingCover);

	// Input
	void InputAxis_MoveForward(float AxisValue);
	void InputAxis_MoveRight(float AxisValue);
	void InputAxis_MousePitch(float AxisValue);
	void InputAxis_MouseYaw(float AxisValue);
	void InputAction_SprintButton();
	void InputAction_CoverButton();
	void InputAction_AimButtonPressed();
	void InputAction_AimButtonReleased();
	void InputAction_FireButtonPressed();
	void InputAction_FireButtonReleased();
	void InputAction_ToggleCamera();
	
	// Cover
	void TakeCover();
	bool CoverAngleIsOk();

	// Zoom
	void SetFOV(bool _isAiming);

	UTimelineComponent* ZoomTimeline;

	UPROPERTY(EditDefaultsOnly, Category = "Timeline")
	UCurveFloat* ZoomCurve = nullptr;

	FOnTimelineFloat ZoomInterpFunction;

	UFUNCTION()
	void ZoomTimelineFloatReturn(float val);

	// Camera
	FVector socketOffset;
	float bCameraIsFacingRight = true;
	void ToggleCameraPosition();

	UTimelineComponent* ToggleCameraTimeline;

	UPROPERTY(EditDefaultsOnly, Category = "Timeline")
	UCurveFloat* ToggleCameraCurve = nullptr;

	FOnTimelineFloat ToggleCameraInterpFunction;

	UFUNCTION()
	void ToggleCameraTimelineFloatReturn(float val);
};
