// Property of Erick Pombo Sonderblohm

#pragma once

#include "GameFramework/Character.h"
#include "DamageInterface.h"
#include "BaseEnemyCharacter.generated.h"

class UPawnSensingComponent;
class AKPlayerCharacter;

UENUM()
enum class ECombatStatus : uint8
{
	Guard,
	Patrol,
	Combat
};

UENUM()
enum class ECombatType : uint8
{
	Assault,
	Cover,
	Heavy
};

UCLASS()
class KARMA_API ABaseEnemyCharacter : public ACharacter, public IDamageInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseEnemyCharacter();


	virtual void TakeDamage(float _DamageMin, float _DamageMax, float _CritDamageMultiplier, float _CritChance, FName HitBoneName, AActor* InstigatorActor) override;


	virtual void PostInitializeComponents() override;


	virtual void OnConstruction(const FTransform& Transform) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AIPerception")
	UPawnSensingComponent* PawnSensingComponent = nullptr;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, Category = "Default")
	ECombatStatus CombatStatus = ECombatStatus::Guard;

	UPROPERTY(EditAnywhere, Category = "Default")
	ECombatType CombatType = ECombatType::Assault;

	UPROPERTY(VisibleAnywhere, Category = "Player")
	AKPlayerCharacter* PlayerCharacter = nullptr;

	UPROPERTY(EditAnywhere, Category = "Waypoint")
	UBillboardComponent* Waypoint_1 = nullptr;

	UPROPERTY(EditAnywhere, Category = "Waypoint")
	UBillboardComponent* Waypoint_2 = nullptr;

	UPROPERTY(EditAnywhere)
	USkeletalMeshComponent* WeaponMesh = nullptr;

private:

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	USkeletalMesh* AssaultWeaponMesh = nullptr;

	UArrowComponent* MuzzleArrow = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	USkeletalMesh* CoverWeaponMesh = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	USkeletalMesh* HeavytWeaponMesh = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Health")
	float CurrentHealth = 0.0f;
	
	UPROPERTY(EditDefaultsOnly, Category = "Health")
	float MaxHealth = 100.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Health")
	bool bIsDead = false;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	FName WeaponSocket = "WeaponSocket";

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	FName MuzzleSocket = "Muzzle";

	FVector Waypoint_1_Location;
	FVector Waypoint_2_Location;

	void CalculateHealth(float _DamageMin, float _DamageMax, float _CritDamageMultiplier, float _CritChance, FName HitBoneName);
	
	UFUNCTION()
	void OnSeePawn(APawn* _Pawn);

	void SetToCombat(APawn* _Pawn);

	UFUNCTION()
	void OnHearNoise(APawn* _Instigator, const FVector& _Location, float _Volume);

	
	
};
