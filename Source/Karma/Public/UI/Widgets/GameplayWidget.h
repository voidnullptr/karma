// Property of Erick Pombo Sonderblohm

#pragma once

#include "Blueprint/UserWidget.h"
#include "GameplayWidget.generated.h"

/**
 * 
 */
UCLASS()
class KARMA_API UGameplayWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeConstruct() override;

	UFUNCTION(BlueprintImplementableEvent, Category = "Event Dispatcher")
	void OnWeaponOverheated(bool IsWeaponOverheated);

	UFUNCTION(BlueprintImplementableEvent, Category = "Event Dispatcher")
	void OnWeaponOverheatedPercent(float WeaponOverheatPercent);

	UFUNCTION(BlueprintImplementableEvent, Category = "Event Dispatcher")
	void OnShowHideCrosshairs(ESlateVisibility CrossHairsVisibility);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Heat")
	bool bIsWeaponOverheated = false;

};
