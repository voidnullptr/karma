// Property of Erick Pombo Sonderblohm

#pragma once

#include "GameFramework/HUD.h"
#include "PlayerHUD.generated.h"

class UUserWidget;

/**
 * 
 */
UCLASS()
class KARMA_API APlayerHUD : public AHUD
{
	GENERATED_BODY()

public:

	APlayerHUD();
	
private:

	UPROPERTY(EditDefaultsOnly, Category = "Widget")
	TSubclassOf<UUserWidget> WP_GameplayWidget;

	UPROPERTY(VisibleAnywhere, Category = "Widget")
	UUserWidget* WI_GameplayWidget = nullptr;
	
protected:
	virtual void BeginPlay() override;

};


