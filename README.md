### What is this repository for? ###

Karma (Working Title) is a tactical 3rd person shooter game in which you have to infiltrate a military lab to steal an important artifact to save the lives of many.
Technical specifications should follow these requirements:

* Cover animations and cover logic for high and low cover
* Physical animations
* AI with 3 states, attack, patrol and guard plus different types of AI
* Rifle weapons and the ability to add different equipment ingame
* The whole game must be implemented on C++ salvo the logic for the animation graph and AI state machine
* Last but not least, the ability to play in VR

### Contact details and more information about me ###

*Erick Sonderblohm*

* sonderblohm.dev@gmail.com
* [LinkedIn](https://www.linkedin.com/in/erick-pombo-sonderblohm-49883a58/)
* https://github.com/nullB0t
* http://sonderblohm.net/